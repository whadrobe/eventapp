(function($){
  $(function(){

    $('.button-collapse').sideNav();

  }); // end of document ready
})(jQuery); // end of jQuery name space


/**
 * This function empties the ticket fields and repopulate
 * @param {string} param
 */
$("#activate_ticket").change(
	function(){//
        //get initial contents(Fields) in the div and keep it
         initialContent=$('#add_ticket').html();
     
         //toggle
	$('#add_ticket').toggle(function(){
            //empty it
            $(this).empty();
            console.log('Emptied');
        },function(){
            //fill it with the innitial feilds
            $(this).html(initialContent);
            
            console.log('Container Populated');
        })
}
)

function deleteEvent(event_number){
        $.ajax({
            url:'/event/delete/'.event_number,
            datatype: "post",
            cache:false,
            before:alert('You are about to delete this Event'),
            success: function(msg){
                console.log(msg);
            },
            error: function(){
                console.log("ERror")
            },
        })
    }
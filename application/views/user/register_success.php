<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row">
    <div class=" col-sm-6 white">
    <p> Your registration is successful. Please check your email 
        <strong> <?php echo html_escape($user->get_user_email()) ?> </strong>
    and follow the activation link to activate your account </p>
    </div>
</div>
<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<section class="section">
    <div class='row centered'>

    <div class="col s10 m8 l4 offset-l4 offset-m2 form-container">
        <h3 class="center thin">Login</h3>
        <div class="toast-container">
            <?php 
            echo validation_errors(); 
            if(isset($info)){
                echo $info;
            }
            ?>
        </div>
        <div class="form">
        <?php echo form_open("user/login");?>
            <div class="input-field">  
        <?php echo form_label("Email", "user_email");?>
        <?php echo form_input(array('type'=>'email', 'name'=>'user_email','class'=>'validate','value'=> set_value('user_email'),'required'=>'')); ?>
            </div>
            <div class="input-field">
        <?php echo form_label("Password", "user_password");
        echo form_input(array('type'=>'password', 'name'=>'user_password','class'=>'validate','required'=>''));
           ?>
            </div>
            <div class="input-field center">
        <?php
        echo form_input(array('type'=>'submit', 'name'=>'user_login',"value"=>'Send','class'=>'btn blue'));
        ?>
            </div>
        <?php echo form_close();?>
        </div>
        
        
    </div>
    
</div>
</section>

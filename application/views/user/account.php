<div class="container">
    <h3>Account Settings</h3>
    <div class="row">
        <div class="col s3">
        
        <?php $this->load->view('includes/account_side_nav'); ?>
        </div>

        <div class="col s9 form-container " style="margin-top:0">

            <?php echo validation_errors() ?>
            <?php if(isset($info)){ echo  $info;} ?>
            <div class="row">
                <div class="">
                    <div class="row">
                        <form class="col s8 offset-s2"  method="POST" action="<?php echo site_url('user/update_account_info')?>" enctype="multipart/form-data">
                            <div class="row">

                                <div class="col s12 center">
                                    <div class="file-field input-field">
                                        <div>
                                          <img width="70px" data-target="" src="<?php echo base_url('assets/images/svg/user_add_image.svg') ?>">
                                          <input name="user_photo"  id="user_photo" type="file" accept=".jpg,.png" value="<?php set_value('user_photo') ?>" capture>
                                        </div>
                                        <div class="file-path-wrapper">
                                          <input class="file-path validate" type="text" placeholder="Upload one or more files">
                                        </div>
                                    </div>
                                </div>
                            </div>
                          <div class="row">
                            <div class="input-field col s6">
                                <input placeholder="Your firstname" name="user_firstname" id="user_firstname" type="text" value="<?php echo $user->user_firstname ?>" class="validate">
                              <label for="user_firstname">First Name</label>
                            </div>
                            <div class="input-field col s6">
                              <input id="user_lastname" name="user_lastname"type="text"  placeholder="Your lastname" value="<?php echo $user->user_lastname ?>" class="validate">
                              <label for="user_lastname">Last Name</label>
                            </div>
                          </div>
                            
                            <div class="row">
                                <div class="input-field col s6">
                                    <i class="fa fa-at prefix "></i>
                                    <input placeholder="Eg.someone@mail.com" name="user_email" id="user_email" type="text" value="<?php echo $user->user_email ?>" class="validate" disabled="">
                                    <label for="user_organisation">Email</label>
                                </div> 
                                <div class="input-field col s6">
                                    <i class="fa fa-university prefix "></i>
                                    <input placeholder="Your organisation can be here" name="user_organisation" id="user_organisation" type="text" value="<?php echo $user->user_organisation ?>" class="validate">
                                    <label for="user_organisation">Organisation</label>
                                </div> 
                            </div>
                          
                          <div class="row">
                            <div class="input-field col s12">
                                <i class="fa fa-phone prefix "></i>
                                <input id="user_phone" name="user_phone" type="tel" class="validate" value="<?php echo $user->user_phone ?>">
                              <label for="user_phone">Phone</label>
                            </div>
                          </div>
                          <div class="row">
                            <div class="input-field col s6">
                                <i class="fa fa-building prefix "></i>
                                <input id="user_location_city" name="user_location_city" type="text" class="validate" placeholder="Which city are you based" value="<?php echo $user->user_city ?>">
                              <label for="user_location_city">City</label>
                            </div>
                              <div class="input-field col s6">
                                  <i class="fa fa-road prefix "></i>
                                <input id="user_location_province" name="user_location_province" type="text" class="validate" placeholder="Your town,area or street" value="<?php echo $user->user_province ?>">
                              <label for="user_location_province">Province</label>
                            </div>
                          </div>
                            <div class="row">
                            <div class="input-field col s12">
                                <i class="fa fa-globe prefix "></i>
                                <input id="user_website" name="user_website" type="text" class="validate" placeholder="htt://yourdomain.com" value="<?php echo $user->user_website ?>">
                              <label for="user_website">Website</label>
                            </div>
                              
                          </div>
                            <input name="update_user_info" value="Update" class="btn btn-block center-block" type="submit">
                        </form>
                      </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <h2>Change Password</h2>
        <div class="col s3">
        
        
        <?php $this->load->view('includes/account_side_nav'); ?>
        </div>
       

        <div class="col s6 offset-s1 form-container" style="margin-top: 0">
            <form method="post" action="change_password">
                <div class="row">
                  <div class="input-field col s12">
                      <input id="prev_password" name="prev_password" type="password" class="validate" required>
                    <label for="password">Current Password</label>
                  </div>

                    <div class="input-field col s12">
                    <input id="new_password" name="new_password" type="password" class="validate" required>
                    <label for="password">New Password</label>
                  </div>

                    <div class="input-field col s12">
                        <input id="new_password_confirm" name="new_password_confirm"  type="password" class="validate" required>
                    <label for="password">Confirm New Password</label>
                  </div>
                </div>
                <input type="submit" name="user_change_password" value="Change" class="btn" >
            </form>
            
            <?php echo validation_errors() ?>
            <?php if(isset($info)){ echo  $info;} ?>
        </div>
  </div>

</div>


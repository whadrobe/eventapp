<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title> <?php echo $page_title ?> | <?php echo APP_NAME ?></title>
    
    <!-- materialize Core CSS -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> 
    <link href="<?php echo base_url('assets/css/materialize.css')?>"rel="stylesheet">
   
    <link href="<?php echo base_url('assets/css/font-awesome/css/font-awesome.min.css')?>"rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url('assets/css/style.css')?>" rel="stylesheet">
    

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
  <header>
      <!-- Navigation -->
      <div class="navbar-fixed">
        <nav class="lighten-1 white">
                <div class="nav-wrapper">
                  <a href="<?php echo base_url()?>" class="brand-logo orange-text"><?php echo APP_NAME ?></a>
                  <ul class="right hide-on-med-and-down orange">

                    <!-- Dropdown Trigger -->
                       <?php if(MY_Controller::is_logged_in()!=TRUE){?>

                        <!-- Dropdown Structure -->
                        <!-- -->
                          <ul id="" class="">
                              <li><a href="<?php echo login_url ?>">Login</a></li>
                              <li><a href="<?php echo sign_up_url ?>">Sign Up</a></li>
                          </ul>
                      <?php } else{?>
                              <li>
                                <a class="dropdown-button" href="#!" data-activates="logged_in_dropdown"> My Account </a>
                                <ul id="logged_in_dropdown" class="dropdown-content">
                                  <li><a href="<?php echo site_url('user/account')?>"> Account Settings</a></li>
                                  <li><a href="<?php echo site_url('event/myevent') ?>" >Events</a></li>
                                  <li><a href="#">Tickets</a></li>
                                  <li role="separator" class="divider"></li>
                                  <li><a href="<?php echo logout_url ?>">Logout</a></li>
                                </ul>
                              </li>
                              <?php } ?>
                  </ul>
                </div>
        </nav>
    
      </div>  
    
  </header>
  <body>
      
      <!-- Generic messages are set here -->
      <?php if(!isset($info)){ $info ; } ?>
      
      
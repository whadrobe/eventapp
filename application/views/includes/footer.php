
<!-- Footer -->
 

  <footer class="page-footer white black-text">
    <div class="container">
      <div class="row">
        <div class="col l6 s12">
          <h5> Copyright &copy;  <?php echo APP_NAME ." ". date('Y') ?></h5>
          <p class="text-lighten-4">Event organising and ticketing has never been this easy. Organize all your events at the lowest possible cost</p>
        </div>

        <!-- Navigation -->
        <!--
        <div class="col l3 s12">
          <h5 class="white-text">Settings</h5>
          <ul>
            <li><a class="white-text" href="#!">Link 1</a></li>
            <li><a class="white-text" href="#!">Link 2</a></li>
            <li><a class="white-text" href="#!">Link 3</a></li>
            <li><a class="white-text" href="#!">Link 4</a></li>
          </ul>
        </div>
        <div class="col l3 s12">
          <h5 class="white-text">Connect</h5>
          <ul>
            <li><a class="white-text" href="#!">Link 1</a></li>
            <li><a class="white-text" href="#!">Link 2</a></li>
            <li><a class="white-text" href="#!">Link 3</a></li>
            <li><a class="white-text" href="#!">Link 4</a></li>
          </ul>
        </div>

        -->
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container black-text">
      Partnership with <a class="text-lighten-3" href="http://whadrobe.com"> Whadrobe.com </a>
      </div>
    </div>
  </footer>


<script src="<?php echo base_url('assets/js/jquery.js')?>"></script>
<script src="<?php echo base_url('assets/js/materialize.js')?>"></script>

<script src="<?php echo base_url('assets/js/plugins/pickadate/picker.time.js')?>"></script>
<script src="<?php echo base_url('assets/js/plugins/pickadate/legacy.js')?>"></script>
<script src="<?php echo base_url('assets/js/init.js')?>"></script>


<script src="<?php echo base_url('assets/js/googleMap_autocomplete.js')?>"></script>
<script>
$( document ).ready(function(){
    //dropdown
    $(".dropdown-button").dropdown();
    
    
    //datepicke
    $('.datepicker').pickadate({
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 15 // Creates a dropdown of 15 years to control year
  });
  
  //timepicker
  $('.timepicker').pickatime()
  
    //Google Map
    // Run the initialize function when the window has finished loading.

 $('.modal-trigger').leanModal();

})

</script>
  </body>
</html>

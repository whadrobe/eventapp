<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!-- Page Content -->
  <div class="section white-text no-pad-bot valign-wrapper" id="index-banner">
      <div class="row">
          <div class="valign-wrapper">
                
                <div class="container">
                 <h1 class=" center orange-text">Sell Tickets</h1>
                <div class="row center">
                  <h5 class="col s12 light">
                      Your ticket and event all at on place. Increase the sales and customer comfortability. 
                      Its easy start NOW! 
                  </h5>
                </div>

                <div class="row center">
                  <a href="<?php echo create_event_url ?>" id="download-button" class="btn-large waves-effect waves-light orange hoverable">Create Event!</a>
                </div>
                </div>
            </div>
      </div>    
  </div>
<li class="divider"></li>
        <section>
        <div class="container">
            <div class="row center-align "> 
                <h5 class="light"> Upcoming Events </h5>
            <!-- /.row -->

           <?php if($events){
                foreach ($events as $event){ ?>
                <div class="col s12 m6 l4">
                  <div class="card small hoverable">
                    <div class="card-image waves-effect waves-block waves-light">
                       <a href="<?php echo site_url('event/e/'.$event->event_number) ?>" >
                      <img class=" responsive-img" src="<?php echo EVENT_IMAGE_PATH.'/'. $event->event_image_thumb ?>">
                       </a>
                    </div>
                      <?php 
                      //check if event is free or not
                      if(empty($event->ticket_id)){?>
                      <div class="btn-floating ticket_status">Free</div>
                      <?php }else{//its not free ?>
                      <button class="btn-floating amber ticket_status "> <?php echo $event->ticket_price ?></button>
                      <?php } ?>
                    <div class="card-content">
                        <div class="card-title grey-text text-darken-4">
                            <a href="<?php echo site_url('event/e/'.$event->event_number) ?>" class="truncate" > <?php echo $event->event_title  ?> </a> <i class=" activator fa fa-ellipsis-v right"></i>
                        </div>
                        <time class="grey-text"> <?php echo date("D. M jS, Y",strtotime($event->event_start_date))?> <span class=""> <?php echo date("g:i A",strtotime($event->event_start_time)) ?></span></time>
                      
                      <div class="grey-text truncate"><?php echo $event->event_location ?></div>
                    </div>
                
                    <div class="card-reveal">
                      <span class="card-title grey-text text-darken-4 ">
                          <?php echo $event->event_title  ?><i class="material-icons right">close</i>
                      </span>
                       
                      <div class="row">
                          <div class="col s12">
                              <p>
                                  <?php $event->event_description ?>
                              </p>
                              <a href="<?php echo site_url('event/e/'.$event->event_number) ?>" class="blue-text"> More Info </a>
                          </div>

                          <div class="col s12">
                              <a href="#" class="  green-text"><i class="fa fa-money"></i> Purchase Ticket! </a>
                          </div>

                      </div>
                    </div>
                  </div>
                </div>
              
            <?php    }
                }
            ?>
            
            <!-- Page Features -->


            </div> <!-- /.row -->
        </div>
      </section>
<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>


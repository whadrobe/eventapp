<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$ci =& get_instance();
$class=$ci->router->fetch_class();
?>
<div class="event-detail-head" style="background-image:-webkit-linear-gradient(top, rgba(0,0,0,0.6), rgba(0,0,0,0.2)), url('<?php echo EVENT_IMAGE_PATH.'/'. $event->event_image_name ?>')">
    <div class="row">
        <div class="col m12 center">

        <h1 class=" white-text "><?php echo $event->event_title ?></h1>
            <p class="white-text  darken-2">
            <i class="fa fa-calendar" aria-hidden="true"></i> <?php echo date("D. M jS, Y  g:i A", strtotime($event->event_start_date .$event->event_start_time)) ?> <br> 
            <i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo  $event->event_location ?> </p>
            <?php if(!empty($event->user_organization)){ ?>
            <p> Organised by: <?php echo $event->user_organization ?> </p>
            <?php } ?>
        </div>
    </div>
</div>
<section >
    <div class="row">
        <div class="col s10 offset-s1 white" style="position:relative;top:-50px; border-radius: 5px">
        <div class="row" style="height:50px;position:relative;top:0px">
            
             <div class="center-align">
                 <?php if(!empty($event->ticket_price)){?>
                  <button class="btn blue white-text waves-ripple" style="border-radius:0px"> GH¢ <?php echo $event->ticket_price ?> | Buy Ticket</button>
                 <?php }else{?>

                      <!-- Modal Trigger -->
                        <a data-target="register_event" class="waves-effect modal-trigger waves-light btn green white-text">Register</a>
                 <?php } ?>
             </div>
         </div>
            <div class="divider"></div>
            <div  class="row">
            <!--Event Description -->

                <div class="col s6">
                    <h5>Event Detail</h5>
                    <div class="divider"></div>

                    <h6 class="grey-text">Description </h6>
                    <p>
                        <?php echo $event->event_description ?>
                    </p>
                    <div class="divider"></div>
                    <h6 class="grey-text">Time & Venue </h6>
                    <p><i class="fa fa-clock-o"></i> <?php echo date("D. M jS, Y  g:i A", strtotime($event->event_start_date .$event->event_start_time)) ?><br></p>
                </div>

                <div class="col s6">
                    <!-- Location Map-->
                    <div>
                        <p><i class="fa fa-map-marker"></i> <?php echo $event->event_location ?></p>
                        <iframe id="locate" width="100%" height="350px" 

                            frameborder="0" style="border:0"
                            src="https://www.google.com/maps/embed/v1/place?key=AIzaSyAPpALUiNv4nHPJ6mKg2x6i4scoVv166IY
                            &q=<?php echo $event->event_location ?>">
                    </iframe>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</section>

<scetion>
  <!-- Modal Structure -->
  <div id="register_event" class="modal">
    <div class="modal-content">
        <h4>Register- <?php echo htmlentities($event->event_title) ?></h4>
        <div class=" form-container row">
            <?php echo form_open($class."/register");?>
            <input class="event_number" type="hidden" name="event_number" value="<?php echo $event->event_number ?>">

            <div class="input-field">
                    <input class="" type="text" name="r_name" placeholder="Eg. Andrew Chamamme">
                    <label for="r_name">Full Name </label>
                </div>
                <div class="input-field">
                    <input class="" type="email" name="r_email" placeholder="Eg. someone@example.com">
                    <label for="r_email">E-mail</label>
                </div>
                <div class="input-field">
                    <input class="" type="tel" name="r_phone" placeholder="Eg. 0267979001">
                    <label for="r_phone">Phone</label>
                </div>
            
                <div class="modal-footer">
                    <button type=" submit" class=" modal-action modal-close waves-effect waves-green btn-flat">Submit</button>
                </div>
                
            <?php echo form_close();?>
            
        </div>
    </div>
    
  </div>
</scetion>




<div class="container">
    <h3> My Events</h3>
    
     <div class="row">
        <div class="col s12">
          <ul class="tabs">
              
            <li class="tab col s3"><a class="active" href="#all_events_tab">All</a></li>
            <li class="tab col s3"><a  href="#upcoming_events_tab">Up-coming</a></li>
            <li class="tab col s3"><a href="#past_events_tab">Past</a></li>
            
          </ul>
        </div>
        
         <div class="container">
             <div id="all_events_tab" class="col s12">
                <div class="row ">
                     <div class="col s12 m12">
                     <ul class="collection">
                     <?php

                     //check if there are events
                         if($events!=NULL){
                             //Display each eents

                             foreach ($events as $event){ ?>

                                     <li class="collection-item avatar">
                                       <img src="<?php echo EVENT_IMAGE_PATH.'/'.$event->event_image_name ?>" alt="" class="circle">
                                       <span class="title"><?php echo $event->event_title ?></span>
                                       <p class="grey-text">
                                           <?php echo date("D. M jS, Y  g:i A", strtotime( $event->event_start_date . $event->event_start_time)) ?> - <?php echo date("D. M jS, Y  g:i A", strtotime($event->event_end_date .$event->event_end_time))  ?> <br>
                                           <a class="blue-text" href="<?php echo site_url('event/edit/'. $event->event_number) ?>"><i class="fa fa-edit"></i> Edit</a> 
                                           <a class=" red-text" href="<?php echo site_url('event/delete'.'/'.$event->event_number) ?>" onclick="deleteEvent('<?php echo $event->event_number  ?>')"><i class="fa fa-trash"></i> Delete</a>
                                       </p>
                                       <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
                                     </li>


                         <?php }}?>
                     </ul>
                     </div>
                 </div> 
        </div>
        <div id="upcoming_events_tab" class="col s12">
            <?php include "__upcomingEvents.php" ?>
        </div>
        <div id="past_events_tab" class="col s12">
            <?php include "__pastEvents.php" ?>
        </div>
         </div>
        
       
      </div>
 
</div>



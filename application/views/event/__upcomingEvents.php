
<?php 
$user_id=$this->session->userdata('user_id');
$upcomingEvents=Event_m::get_user_upcomingEvent($user_id);       
?>

 <div class="row">
    <div class="col s12 m12">
    <ul class="collection">
    <?php

    //check if there are events
        if($upcomingEvents!=NULL){
            //Display each eents

            foreach ($upcomingEvents as $event){ ?>

                    <li class="collection-item avatar">
                      <img src="<?php echo EVENT_IMAGE_PATH.'/'.$event->event_image_name ?>" alt="" class="circle">
                      <span class="title"><?php echo $event->event_title ?></span>
                      <p class="grey-text">
                          <?php echo date("D. M jS, Y  g:i A", strtotime( $event->event_start_date . $event->event_start_time)) ?> - <?php echo date("D. M jS, Y  g:i A", strtotime($event->event_end_date .$event->event_end_time))  ?> <br>
                          <a class="blue-text" href="<?php echo site_url('event/edit/'. $event->event_number) ?>"><i class="fa fa-edit"></i> Edit</a> 
                          <a class=" red-text" href="#"><i class="fa fa-trash"></i> Delete</a>
                      </p>
                      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
                    </li>


        <?php }

        }else{?>
                    <p class="">You have no upcoming event</p>
        <?php }?>
    </ul>
    </div>
</div> 
<div class="row">
    <?php echo form_open_multipart("event/create");?>
    <div class="col s8 offset-s2 evt_form">
        <div class="">
            <?php echo validation_errors();?>
        </div>
        <h3>Event Details</h3>

      <div class="row">
        <div class="input-field col s6">
                <?php echo form_label("Event Title", "event_title");?>
                <?php echo form_input(array('type'=>'text','id'=>"event_title", 'name'=>'event_title','class'=>'validate ','required'=>'','value'=> set_value('event_title',$this->input->post('event_title')))); ?>
        </div>
        
      </div>
        <div class="row">
            <div class="input-field col s6">
                <?php echo form_label("Description", "event_description");
                echo form_textarea(array('name'=>'event_description','placeholder'=>'Gist us about event','class'=>'materialize-textarea','required'=>'','value'=> set_value('event_description',$this->input->post('event_description'))));
                   ?>
            </div>
        </div>
      <div class="row">
        <div class="input-field col s12">
           <?php echo form_label("Location", "event_title");?>
             <?php echo form_input(array('type'=>'text','id'=>'event_location', 'name'=>'event_location','placeholder'=>'Where will the event be held','required'=>'','class'=>'validate','value'=> set_value('event_location',$this->input->post('event_location')))); ?>   
        </div>
      </div>
        
        <div class="row">
            
            <div class="col s6">  
                <div class="row">
                    <h5>Starts</h5>
                    <div class=" input-field  col s6">
                        <?php echo form_label("Date", "event_start_date");?>
                        <?php echo form_input(array('class'=>'datepicker','type'=>'date', 'name'=>'event_start_date','placeholder'=>'Date','required'=>'')); ?>
        
                    </div>
                    <div class=" input-field col s4">
                        <?php echo form_label("Time", "event_start_time");?>
                        <?php echo form_input(array('class'=>'timepicker','type'=>'time', 'name'=>'event_start_time','placeholder'=>' At what time','required'=>'')); ?>
                    </div>
                </div>
            </div>
            <div class="col s6">  
                <div class="row">
                    <h5>Ends</h5>
                    <div class=" input-field  col s6">
                        <?php echo form_label("Date", "event_end_date");?>
                        <?php echo form_input(array('class'=>'datepicker','type'=>'date', 'name'=>'event_end_date','placeholder'=>'Date','required'=>'')); ?>
        
                    </div>
                    <div class=" input-field col s4">
                        <?php echo form_label("Time", "event_end_time");?>
                        <?php echo form_input(array('class'=>'timepicker','type'=>'time', 'name'=>'event_end_time','placeholder'=>' At what time','required'=>'')); ?>
                    </div>
                </div>
            </div>
        </div>
      <div class="row">
        <div class="input-field col s12">
            
             <form action="#">
                <div class="file-field input-field">
                  <div class="btn">
                    <span><i class="fa fa-image"></i> Image</span>
                    <input type="file" multiple>
                  </div>
                  <div class="file-path-wrapper">
                      <?php echo form_input(array('type'=>'file','class'=>"file-path validate", 'name'=>'event_image','placeholder'=>"Upload image")); ?>
              
                  </div>
                </div>
              </form>
            
        </div>
      </div>
        <!-- Ticketing -->
        <section class="section">
                <h3> Tickets </h3>

 
                <!-- Switch -->
                    <div class="switch">
                      <label>
                        None
                        <input type="checkbox" name="has_ticket" id="activate_ticket">
                        <span class="lever"></span>
                        Add Ticket(s)
                      </label>
                    </div>
                
                   <div  class="row" id="add_ticket" style="display:none">
                        <div class="collapse" >
                            <div class="row">
                                <div class="input-field col s4">
                                    <div class="file-field input-field">  
                                        <?php echo form_label("Ticket Name", "ticket_name");?>
                                        <?php echo form_input(array('type'=>'text', 'name'=>'ticket_name','class'=>'validate','value'=> set_value('ticket_name',$this->input->post('ticket_name')))); ?>
                                    </div>
                                </div>
                                <div class="input-field col s4">
                                    <div class="file-field input-field">  
                                        <?php echo form_label("Quantity", "ticket_quantity");?>
                                        <?php echo form_input(array('type'=>'text', 'name'=>'ticket_quantity','class'=>'validate','value'=> set_value('ticket_quantity',$this->input->post('ticket_quantity')))); ?>
                                    </div>
                                </div>
                                <div class="input-field col s4">
                                    <div class="file-field input-field">  
                                        <?php echo form_label("Price (GHS)", "ticket_price");?>
                                        <?php echo form_input(array('type'=>'text', 'name'=>'ticket_price','class'=>'validate','value'=> set_value('ticket_price',$this->input->post('ticket_price')))); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                
                <div class="row">
                    <i class="divider"></i>
                    <div class="input-field">
                    <?php echo form_input(array('type'=>'submit', 'name'=>'event_save',"value"=>'Save','class'=>'btn btn-success'));
                    ?>
                        </div>
                </div>
                     
                   
            </section>
    </div>
    <?php echo form_close();?>
  </div>

<script>
      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      function initMap() {
        
        var input = /** @type {!HTMLInputElement} */(
            document.getElementById('event_location'));

        var autocomplete = new google.maps.places.Autocomplete(input);
       

        autocomplete.addListener('place_changed', function() {
         
         
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            window.alert("Autocomplete's returned place contains no geometry");
            return;
          }

          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            console.log(place.geometry);
              console.log(place.geometry.location);
              
          } else {
           console.log('No view pport')
          }
            
          var address = '';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }
          
        });
      }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAPpALUiNv4nHPJ6mKg2x6i4scoVv166IY&libraries=places&callback=initMap"
        async defer></script>
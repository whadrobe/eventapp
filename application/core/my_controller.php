<?php
class MY_Controller extends CI_Controller{
    public function __construct() {
        parent::__construct();

        // Displaying Errors In Div
        $this->form_validation->set_error_delimiters('<div class="toast toast-center red accent-1">', '</div>');

        //Project Detail
        define('APP_NAME', 'Synops');
        //site urls
        define('login_url', site_url('user/login/'));
        define('logout_url', site_url('user/logout/'));
        define('sign_up_url', site_url('user/register/'));
        
        ////Event
        define('create_event_url', site_url('event/create/'));
        //image paths
        define('image_upload_path', base_url('assets/uploads/'));
        define('EVENT_IMAGE_PATH', base_url('assets/uploads/event/'));
       
        
        //Account
        define('ACCOUNT_INFOMATION_URL', site_url('user/account/'));
        define('CHANGE_PASSWORD_URL', site_url( 'user/change_password/'));
    }
    
   
    
    public static function is_logged_in(){
        $CI =& get_instance();
        if($CI->session->userdata('is_logged_in')){
			return TRUE;
        }else{
            return FALSE;
        }
                        
    }
    

}
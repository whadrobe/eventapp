<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('Event_m');
        
        
    }
	public function index()
	{   
            $data['page_title']='Home';
            $data['main_view']='home';
            
            //get events
            $data['events']=  Event_m::get_all_events();
         
            $this->load->view('includes/template',$data);
	}
}

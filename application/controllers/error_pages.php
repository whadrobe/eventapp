<?php

Class Error_pages extends MY_Controller{
     public function _404(){
         $data['page_title']='404-page not found';
         $data['main_view']='404';
         $this->load->view('includes/template',$data);
     }
}
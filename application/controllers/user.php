<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php

class User extends MY_Controller{
    
    public function __construct() {
        parent::__construct();
        $this->load->model('user_m');
    }


    public function index(){
        //check if the user is logged in
        //If logged in then reidirect to the login page
        $this->account();       
    }
    
    public function login(){
        $data['page_title']="Login";
        $data['main_view']="user/login_form";
        //check if the user is already already logged in then send the user to the account page
        if($this->session->userdata('is_logged_in')==TRUE){
            redirect('user/account');
        }else
        //-------------------------------------------------------------------------------------
        if (!empty($this->input->post("user_login"))){
            

            if($this->form_validation->run('login')){
                $user_name= $this->input->post("user_name");
                $user_email= $this->input->post("user_email");
                $user_password= $this->input->post("user_password");

                //Instantiate user Class
                $login_user=new User_m;

                //set propeties for verification
                $login_user->set_user_name($user_name);
                $login_user->set_user_email($user_email);
                $login_user->set_user_password($user_password);
                
              
            if($login_user->login()){
                //get user session data
                //redirect('/user/account');
                if(!empty($this->session->userdata('reff_url'))){
                    redirect($this->session->userdata('reff_url'));
                }else{
                    $this->account();
                }
                
                
                
            }else{
                $data['info']= ' <div class="toast toast-center red accent-1"> Email or password not correct </div>';
                
                $this->load->view('includes/template',$data);
            }
            
            }else{
               $this->load->view('includes/template',$data);
            }
        
        }  else {
            $this->load->view('includes/template',$data);
        }
    }


    public function register(){
        //Assign default view
        $data['page_title']="Signup";
        $data['main_view']='user/register_form';
        
        //Check if user is registering else send to the register page
        if(empty($this->input->post("user_submit"))){
            $this->load->view('includes/template',$data);
        }else{
                
                if($this->form_validation->run('user_register')){

                $user_email= $this->input->post("user_email");
                $user_password= $this->input->post("user_password");
                
                
                //instantiate a new user model
                $user= new User_m();
                
                // set user prperties using the methods
                $user->set_user_email($user_email);
                $user->set_user_password($user_password);
                
                //set user data to be passed to views
                $data['user']=$user;
                
                //Validations
                if($user->user_email_exist($user->get_user_email())){
                        $info='<div class="toast toast-center red accent-1"> Email already in use. Please login if you own it</div>';
                        array_push($user->activity_log, $info);
                    //Show the register form if user email and/or username alredy exist
                    $data['info']=$info;
                    $this->load->view('includes/template',$data);
                }
                else{
                    $create = $user->insert_user();
                    if($create){
                        $data['main_view']='user/register_success';
                        $this->load->view('includes/template',$data);

                    }else{
                        echo "Error".mysqli_errno();
                    }
                }
                }else{
                    $this->load->view('includes/template',$data);
                }
            }
            
         }
         public function logout(){
              $this->session->unset_userdata('user_id');
              $this->session->unset_userdata('user_email');
              $this->session->unset_userdata('is_logged_in');
              
              redirect();
         }
    public function user_form(){
        $this->load->helper("form");
        $this->load->view("userform");     
    }
    
    public function account(){
        $data['page_title']="My Account";
        $data['main_view']="user/account";
        
        $user_id= $this->session->userdata('user_id');
        $data['user']= User_m::get_all_details($user_id) ;
        
        $is_logged_in=$this->session->userdata('is_logged_in');
        if ($is_logged_in==TRUE){
            $this->load->view('includes/template',$data);
        }
        else{
            redirect ('user/login');
        }
    }
    
    
    public function update_account_info(){
            
            $data['page_title']='Information';
            $data['main_view']='user/account';
            
            $user_id= $this->session->userdata('user_id');
            
        //Check if a user_uodate form has been submitted
        if($this->input->post('update_user_info')){//Yes its submitted

            //Check if the validation is successful
            if($this->form_validation->run('user_info')){//Oh yeah! Validation success
                
                //Instantiate a new user and populate its attirbutes with the Form iput values
                
                $up_user= new User_m();
                
                // Assign values to the attribute
                $up_user->set_user_firstname($this->input->post('user_firstname'));
                $up_user->set_user_lastname($this->input->post('user_lastname'));
                $up_user->set_user_phone($this->input->post('user_phone'));
                
                $up_user->set_user_city($this->input->post('user_location_city'));
                $up_user->set_user_province($this->input->post('user_location_province'));
                $up_user->set_user_website($this->input->post('user_website'));
                $up_user->set_user_organisation($this->input->post('user_organisation'));
                $up_user->set_user_image_name($this->input->post('user_image_name'));
                
                
                // well we are good to update
                //get the user id
                $user_id=  $this->session->userdata('user_id');

                if($up_user->save($user_id)){
                    $data['info']='<div class="toast toast-center green accent-3">Your account information has been updated</div>';
                }else{
                    $data['info']='<div class="toast toast-center red accent-1"> Sorry Operation Failed </div>';
                }
                
                
                
            }
            
        }
        //get user details
        $data['user']= User_m::get_all_details($user_id) ;
        
        $this->load->view('includes/template',$data);
       
    }
    
    public function change_password(){
        //set data to be passed to the views
        $data['page_title']='Change Password';
        $data['main_view']='user/change_password';
        
        //check if user is logged in
        if(MY_Controller::is_logged_in()){//Logged in
            //check if a form is submitted
            if($this->input->post('user_change_password')){
            
                 if($this->form_validation->run('change_password')){//validation successfull

                     $user =new User_m;
                     
                     //get user id from session
                     $user_id=  $this->session->userdata('user_id');
                     
                     //set attributes
                     $user->previous_password=  $this->input->post('prev_password');
                     $user->new_password=  $this->input->post('new_password');
                     
                     //now change password
                    if($user->change_pasword($user_id)){
                        $data['info']='<div class="toast toast-center green accent-2"> Password changed</div>'; 
                    }else{
                         $data['info']='<div class="toast toast-center red accent-1"> Oops! Our bad.Please try again </div>';
                    }
                 }
                 
            }
            

            $this->load->view('includes/template',$data);
        }else{
            //set referrer
           // $this->session->set_userdata('reff_url','change_password');
            $this->login();
        }
       
    }
    
    static public  function count_events($id){
        $CI =& get_instance();
        $data=array('fk_user_id'=> intval($id));
         $query=$CI->db->get('event',$data);
        return $query->num_rows();
    }
    
}

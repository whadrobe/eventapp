<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Ticket_m extends CI_Model{
    const DB_TABLE_NAME='tickets';
    const DB_TABLE_PK='ticket_id';
    
    public $ticket_number;
    public $ticket_name;
    public $ticket_price;
    public $quantity_available;
    public $fk_event_id;
    //public $fk_user_id;
    public $valid_from;
    public $valid_to;
    
    private $date_created;
    
    
    /**
     * Creates tickets
     * @return boolean
     */
    private function create(){
        $data=array(
            'fk_event_id'=>  $this->fk_event_id,
            'ticket_number'=>  random_string('numeric', 10),
            'ticket_name'=>  $this->ticket_name,
            'ticket_price'=>  $this->ticket_price,
            'quantity_available'=>  $this->quantity_available,
            'valid_from'=>  $this->valid_from,
            'valid_to'=>  $this->valid_to,
        );
        
        $query=$this->db->insert($this::DB_TABLE_NAME,$data);
        if($query){
            return TRUE;
        }else{
            return $query->error;
        }
    }
    
    /*
     * Update a ticket bye id
     * @param type int
     * @return boolean
     */
    private function update($ticket_id){
        $data=array(
            'ticket_name'=>  $this->ticket_name,
            'quantity_available'=>  $this->quantity_available,
            'ticket_price'=>  $this->ticket_price,
            'valid_from'=>  $this->valid_from,
            'valid_to'=>  $this->valid_to,
        );
        $this->db->where('ticket_id',$ticket_id);
        $query=$this->db->update($this::DB_TABLE_NAME,$data);
        if($query){
            return TRUE;
        }else{
            return $query->error;
        }
    }
    
    /**
     * Saves event
     * if event id is sett the action=update
     * else insert
     * @param type int
     */
    public function save($event_id=NULL){
        if(isset($event_id)){
            $this->update($event_id);
        }else{
            $this->create();
        }
    }
    
    public function get($event_id){
        $this->db->where('fk_event_id',$event_id);
        $this->db->get($this::DB_TABLE_NAME);
    }
}

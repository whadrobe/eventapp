<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php

class User_m extends CI_Model{
    /**
     * Constants for Database query;
     */
    const DB_TABLE_NAME="user";
    const  DB_TABLE_PK="user_id";
    
    private $user_name;
    private $user_firstname;
    private $user_lastname;
    private $user_email;
    private $user_password;
    private $user_phone;
    private $user_organisation;
    private $user_city;
    private $user_province;
    private $user_website;
    private $user_image_name;
    
    //Password change attributes
    public $previous_password;
    public $new_password;

    public $activity_log=array();
    
    /**
     * Set name of the user
     * @param string $name
     */
    public function set_user_name($name){
        $this->user_name=html_escape($name);
    }


    /**
     * Set user's Firstname
     * @param $firstname
     * @internal param string $name
     */
    public function set_user_firstname($firstname){
        $this->user_firstname=  html_escape($firstname);
    }


    /**
     * Set user's Firstname
     * @param $lastname
     * @internal param string $name
     */
    public function set_user_lastname($lastname){
        $this->user_lastname=  html_escape($lastname);
    }
    
    /**
     * Password
     * @param string $password
     */
    public function set_user_password($password){
        $this->user_password= sha1($password);
    }
    
    public function set_user_email($email){
        $this->user_email= html_escape($email);
    }
    
    /**
     * Set phone number 1
     * @param int $phone
     */
    public function set_user_phone($phone){
        $this->user_phone=  html_escape($phone);
    }


    /**
     * Set organisation
     * @param $organisation
     * @internal param int $province
     */
    public function set_user_organisation($organisation){
        $this->user_organisation= html_escape($organisation);
    }
    /**
     * Set website 
     * @param int $website_url
     */
    public function set_user_website($website_url){
        $this->user_website= html_escape($website_url);
    }
    
    
    /**
     * Set city 
     * @param int $city
     */
    public function set_user_city($city){
        $this->user_city= html_escape($city);
    }

    /**
     * Set province 
     * @param int $province
     */
    public function set_user_province($province){
        $this->user_province= html_escape($province);
    }

    /**
     * Set Image name 
     * @param int $image_name
     */
    public function set_user_image_name($image_name){
        $this->user_image_name= html_escape($image_name);
    }
    
    
    
    
    
    /**
     * Create User user
     * Inserts user data into database
     * @return boolean
     */
    public function insert_user(){
        $data=array(
            "user_name"=>  $this->user_name,
            "user_firstname"=> $this->user_firstname,
            "user_lastname"=> $this->user_lastname,
            "user_email"=>  $this->user_email,
            "user_password"=>  $this->user_password,
            "user_phone"=> $this->user_phone,
            "user_website"=> $this->user_website,
            "user_organisation"=> $this->user_organisation,
            "user_city"=> $this->user_city,
            "user_province"=> $this->user_province,
            "user_image_name"=> $this->user_image_name
            );
        $this->db->insert($this::DB_TABLE_NAME, $data);
        $this->{$this::DB_TABLE_PK}=$this->db->insert_id();
        
        array_push($this->activity_log,"user created successfully"); 
        return TRUE;
    }
    
    
    public function save($user_id=NULL){
        if(!empty($user_id)){
           return $this->update_user($user_id);
        }else{
            return $this->insert_user();
        }
    }

    /**
     * Upadate User info
     * @param int $id
     * @return bool
     */
    private function update_user($id){
        
        $data=array(
            "user_name"=>  $this->user_name,
            "user_firstname"=> $this->user_firstname,
            "user_lastname"=> $this->user_lastname,
            //"user_email"=>  $this->user_email,
            "user_phone"=> $this->user_phone,
            "user_website"=> $this->user_website,
            "user_organisation"=> $this->user_organisation,
            "user_city"=> $this->user_city,
            "user_province"=> $this->user_province,
            "user_image_name"=> $this->user_image_name
            );
        //udate user table where uuser_id=$id 
        $this->db->where($this::DB_TABLE_PK,$id);
        
        $update=$this->db->update($this::DB_TABLE_NAME, $data);
        
        if($update){
            return TRUE;
        }else{
            return FALSE;
        }
        
    }
    
    /*
    public function save(){
        if(isset($this->{$this::DB_TABLE_PK})){
            $this->update_user($this::DB_TABLE_PK);
        }else{
            $this->insert_user();
        }
    }
    */
    
    
    /**
     * -------------------------------------------------------------------------
     * Getters
     * -------------------------------------------------------------------------
     */
    
   public function get_user_name(){
       return $this->user_name;
   }
   
   public function get_user_full_name(){
       return $this->user_full_name;
   }
   public function get_user_email(){
       return $this->user_email;
   }
   public function get_user_phone_1(){
       return $this->user_phone_1;
   }
   public function get_user_phone_2(){
       return $this->user_phone_2;
   }


    /**
     * --------------------------------------------------------------------------
     * Checkers
     * --------------------------------------------------------------------------
     * @param $a_username
     * @return bool
     */
   public function user_name_exist($a_username){
        $data=array(
           'user_name'=>  $a_username,
           //'user_email'=> $this->user_email,
           //'user_password'=> $this->user_password,
       );
      $query= $this->db->get_where($this::DB_TABLE_NAME,$data);
      if($query->num_rows()>0){
          return TRUE;
      }else{
          return FALSE;
      }
   }
    public function user_email_exist($a_email){
        $data=array(
           'user_email'=>$a_email,
       );
      $query= $this->db->get_where($this::DB_TABLE_NAME,$data);
      if($query->num_rows()>0){
          array_push($this->activity_log, "Email exist");
          return TRUE;
      }else{
          return FALSE;
      }
   }
   
   public function login(){
       //Valdations
       $data= array('user_email'=>  $this->user_email,'user_password'=> $this->user_password);
       $query= $this->db->get_where($this::DB_TABLE_NAME,$data);
       if($query->num_rows() > 0){ 
           $row=$query->row_array();
           
           
           $session_array=array(
               'user_id'=>$row['user_id'],
               'is_logged_in'=>TRUE,
           );
           //set session data
           $this->session->set_userdata($session_array);
           
           array_push($this->activity_log,'Login success');
           
           return TRUE ;
       }else{
           return FALSE;   
       }
    }
    
    
    /**
     * This function updates user password
     * @param type $user_id
     * @return boolean
     */
   
   public function change_pasword($user_id){
       //check if there there is a user with such password
       $data= array(
           self::DB_TABLE_PK=>$user_id,
           'user_password'=> sha1($this->previous_password)
       );
      $query= $this->db->get_where(self::DB_TABLE_NAME,$data);
      
      //check if there is a match
      if($query->num_rows()>0){
          
          //find the match
          $this->db->where(self::DB_TABLE_PK,$user_id);
          
          //set values
          $data= array(
           'user_password'=> sha1($this->previous_password)
            );
          //update
          $query= $this->db->update(self::DB_TABLE_NAME,$data);
          return TRUE;
      }else{
          return FALSE;
      }
   }
   
   
   /**
    * This featches all column of a specific user from the database
    * @param int $user_id
    * @return array
    */
   public static function  get_all_details($user_id){
       $CI=&get_instance();
       $CI->db->select('user_firstname, user_lastname, user_email, user_city, user_province,user_organisation,user_phone,user_website');
       $CI->db->where('user_id',  intval($user_id) );
       $query=  $CI->db->get(self::DB_TABLE_NAME);
       
       return $query->row();
   }
}


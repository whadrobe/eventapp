<?php
class Event_m extends CI_Model{
    public function __construct() {
        parent::__construct();
        $CI =& get_instance();
    }
    
    const DB_TABLE_NAME='event';
    const DB_TABLE_PK='event_id';
    
    public $fk_user_id;
    private $event_number;
    public $event_title;
    public $event_description;
    public $event_location;
    public $event_start_date;
    public $event_start_time;
    public $event_end_date;
    public $event_end_time;
    public $event_price;
    public $event_image_name;
    public $event_image_thumb;
    public $event_active;
    
    public $insert_id;
    
    public function create(){
        $data=array(
           'fk_user_id'=> html_escape($this->fk_user_id),
            'event_number'=>random_string('alnum', 10),
           'event_title'=>  html_escape($this->event_title),
           'event_description'=>  html_escape($this->event_description),
            'event_location'=>  html_escape($this->event_location),
            
            'event_start_date'=>  $this->event_start_date,
            'event_start_time'=>  $this->event_start_time,
            
            'event_end_date'=>   $this->event_end_date,
            'event_end_time'=>   $this->event_end_time,
            
            'event_image_name'=>  html_escape($this->event_image_name),
            'event_image_thumb'=>  html_escape($this->event_image_thumb)
        );

//        var_dump($this->db->insert($this::DB_TABLE_NAME,$data));
        if($this->db->insert($this::DB_TABLE_NAME,$data)){
            //$this->{$this::DB_TABLE_PK}=$this->db->insert_id();
            $this-> insert_id = $this->db->insert_id();
            return TRUE;
        }else{
            return FALSE;
        }
        
    }
    public function update($event_number){
        
        //check if the image filed is empty then dont affect a change to that
        if(empty($this->event_image_name)){
            $data=array(
            'fk_user_id'=> html_escape($this->fk_user_id),
            'event_title'=>  html_escape($this->event_title),
            'event_description'=>  html_escape($this->event_description),
            'event_location'=>  html_escape($this->event_location),
            
            'event_start_date'=>  $this->event_start_date,
            'event_start_time'=>  $this->event_start_time,

            'event_end_date'=>   $this->event_end_date,
            'event_end_time'=>   $this->event_end_time,
            
            'event_price'=>  html_escape($this->event_price),
            
            );
            
        }else{
           
            $data=array(
                'fk_user_id'=> html_escape($this->fk_user_id),
                'event_title'=>  html_escape($this->event_title),
                'event_description'=>  html_escape($this->event_description),
                'event_location'=>  html_escape($this->event_location),

                'event_start_date'=>  $this->event_start_date,
                'event_start_time'=>  $this->event_start_time,

                'event_end_date'=>   $this->event_end_date,
                'event_end_time'=>   $this->event_end_time,

                'event_price'=>  html_escape($this->event_price),
                'event_image_name'=>  html_escape($this->event_image_name),
                'event_image_thumb'=>  html_escape($this->event_image_thumb)  
            );
        }
        
        $this->db->where('event_number',$event_number);
        if($this->db->update($this::DB_TABLE_NAME,$data))
            return TRUE;
        else
            return FALSE;
    }
    
    public function set_event_active($event_id){
        $data=array(
            "event_active"=>1
            );
        //update event table row where event_id=$id 
        $this->db->where($this::DB_TABLE_PK,$event_id);
        ($this->db->update($this::DB_TABLE_NAME, $data))?TRUE:FALSE;
    }
    
    /**
     * I created this function to be used in the
     * @param int $limit
     * @return array
     */
    public function get_event($event_num){
        $event_number= html_escape($event_num);
        $data=array(
            'event_number'=>$event_number
        );
        $query=$this->db->get_where($this::DB_TABLE_NAME,$data);
        if($query->num_rows()==1){
            foreach ($query->result_array() as $e){
                echo $e['event_title'];
            }
        }
        
    }
    
   /**
    * Delete an event row
    * @param string $event_number
    * @return boolean
    */
    
    public static function delete($event_number){
        $CI= & get_instance();
        $event_number=  trim($event_number);
        
        
        //get the event deatail
        $CI->db->select('event_image_name');
        
        
        $query=$CI->db->get(self::DB_TABLE_NAME);
        $result=$query->row();
        
        //delete event_image
      // unlink(base_url('assets/uploads/event/'.$result->event_image_name));
       //unlink(base_url('assets/uploads/event/'.$result->event_image_thumb));
        
        if($query->num_rows()>0){
            $CI->db->where('event_number',$event_number);
            $CI->db->delete(self::DB_TABLE_NAME);
            return TRUE;
        }else{
            return FALSE;
        }
        
        
    }


    public static  function get_all_events($limit=10){
        $CI =& get_instance();
        $CI->db->select('* ');
        $CI->db->from(self::DB_TABLE_NAME.' e ');
       
        $CI->db->join('tickets t','t.fk_event_id= e.event_id','left');
        $CI->db->join('user u','u.user_id= e.fk_user_id');
        $CI->db->order_by('event_id','desc');
        $CI->db->limit($limit);
        $query = $CI->db->get();
        
        if($query->num_rows()>0){        

            return $query->result();
        }else{
            return FALSE;

        }
          
          
}
    /**
     * This function get informations about an event based on parameters given
     * @param type $what
     * @param type $column_name
     * @param type $column_value
     * @return array
     */
    static public function get($what=NULL,$column_name=NULL,$column_value=NULL){
        $CI =& get_instance();

        if(!empty($what)||!empty($column_name)||!empty($column_value)){
            $col_name=  html_escape($column_name);
            $col_val=  html_escape($column_value);

            $data=array($col_name=>$col_val);

            $query=$CI->db->get_where(self::DB_TABLE_NAME,$data);

            if($query->num_rows()>0){
                foreach ($query->result() as $event) {
                    return $event->$what;
                }
               //echo var_dump($result->);

            } 
        }
    
    
    }
    
    /**
     * This function gets all the information or details of a specific event
     * @param type $event_number
     * @return results
     */
    public static function get_event_info($event_number){
        $CI =& get_instance();
       
        $CI->db->select('*');
        $CI->db->from(self::DB_TABLE_NAME.' e');
        $CI->db->where('event_number',$event_number);
        $CI->db->join('tickets t','t.fk_event_id=e.event_id','left');
        $CI->db->join('user u','e.fk_user_id=u.user_id','left');
        $query=$CI->db->get(); 
        
        return $query->row();
    }


    
    /**
     * Returns all events posted by a particular user
     * @param int $user_id
     * @return type
     */
    public static function get_user_event($user_id){
        $CI= & get_instance();
        
        $CI->db->select('*');
        $CI->db->where('fk_user_id',intval($user_id));
        $CI->db->from(self::DB_TABLE_NAME.' e'); 
        $CI->db->join('tickets t','t.fk_event_id = e.event_id','left');
        $query=$CI->db->get();
        
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return NULL;
        }
        
    }
    
    
     /**
     * Returns all upcoming events posted by a particular user
     * @param int $user_id
     * @return type
     */
    public static function get_user_upcomingEvent($user_id){
        $CI= & get_instance();
        
        $CI->db->select('*');
        $CI->db->where('fk_user_id',intval($user_id));
        $CI->db->where('event_start_date > ', date('Y-m-d'));
        $CI->db->from(self::DB_TABLE_NAME.' e'); 
        $CI->db->join('tickets t','t.fk_event_id = e.event_id','left');
        $query=$CI->db->get();
        
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return NULL;
        }
        
    }
    /**
     * Returns all upcoming events posted by a particular user
     * @param int $user_id
     * @return type
     */
    public static function get_user_pastEvent($user_id){
        $CI= & get_instance();
        
        $CI->db->select('*');
        $CI->db->where('fk_user_id',intval($user_id));
        $CI->db->where('event_end_date < ', gmdate('Y-m-d'));
        $CI->db->from(self::DB_TABLE_NAME.' e'); 
        $CI->db->join('tickets t','t.fk_event_id = e.event_id','left');
        $query=$CI->db->get();
        
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return NULL;
        }
        
    }
    /**
     * Checks if there is a similar event
     * @param string $title
     * @param string $location
     * @param date $date
     * @return boolean
     */
    public function event_already_exist(){
        $data=array(
            'event_title'=>  $this->event_title,
            'event_location'=> $this->event_location,
            'event_data'=>  $this->event_date,
        );
        
        $this->db->like($this->event_title,$data);
        
        $query = $this->db->get($this::DB_TABLE_NAME);
        //if the raw returned is true the event already exist
        if($query->num_rows()>0){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    /**
     * Delete event by id
     * @param type $event_id
     * @return bool
     */
    public function delete_event($event_id){
        $data=array('event_id'=>$event_id);
        if($this->db->delete($this->DB_TABLE_NAME,$data)){
            return TRUE;
        }else{
            return FALSE;  
        }      
    }
}


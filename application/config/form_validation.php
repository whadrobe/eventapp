<?php 

$config =array(
    'create_event'=>array(
                array(
                'field' => 'event_title',
                'label' => 'Event Title',
                'rules' => 'required|min_length[3]|max_length[32]'
                    ),
                array(
                        'field' => 'event_description',
                        'label' => 'Event Description',
                        'rules' => 'required|min_length[3]|max_length[1000]'
                ),
                array(
                        'field' => 'event_start_time',
                        'label' => 'Event starting time',
                        'rules' => 'required'
                ),
                array(
                        'field' => 'event_start_date',
                        'label' => 'Event starting dates',
                        'rules' => 'required'
                ),
                array(
                        'field' => 'event_end_date',
                        'label' => 'Event ending date',
                        'rules' => 'required'
                ),
                array(
                        'field' => 'event_end_time',
                        'label' => 'Event ending time',
                        'rules' => 'required'
                ),
                
        ),
    'add_tickets'=>array(
            array(
                        'field' => 'ticket_name',
                        'label' => 'Ticket Name',
                        'rules' => 'required|min_length[3]|max_length[32]'
                ),
                array(
                        'field' => 'ticket_quantity',
                        'label' => 'Ticket Quantity',
                        'rules' => 'required|integer'
                ),
                array(
                        'field' => 'ticket_price',
                        'label' => 'Ticket Price',
                        'rules' => 'decimal'
                )
            ),
    'login'=>array(
            array(
                        'field' => 'user_email',
                        'label' => 'Email',
                        'rules' => 'required|valid_email'
                ),
                array(
                        'field' => 'user_password',
                        'label' => 'Password',
                        'rules' => 'required'
                )
            ),
    'user_register'=>array(
            array(
                        'field' => 'user_email',
                        'label' => 'Email',
                        'rules' => 'required|valid_email'
                ),
            array(
                        'field' => 'user_password',
                        'label' => 'Password',
                        'rules' => 'required'
                )
            ),
    'user_info'=>array(
            array(
                        'field' => 'user_firstname',
                        'label' => 'Firstname',
                        'rules' => 'trim|required|alpha'
                ),
            array(
                        'field' => 'user_lastname',
                        'label' => 'Lastname',
                        'rules' => 'trim|required|alpha'
                ),
            array(
                        'field' => 'user_city',
                        'label' => 'City',
                        'rules' => 'alpha|trim'
                ),
            array(
                        'field' => 'user_phone',
                        'label' => 'Phone',
                        'rules' => 'trim|numeric'
                ),
            array(
                        'field' => 'user_website',
                        'label' => 'Website',
                        'rules' => 'valid_url'
                )
            ),
    'change_password'=>array(
            array(
               'field'=>'prev_password',
                'label'=>'Previous password',
                'rules'=>'trim|required|min_length[6]|max_length[32]'
            ),
            array(
               'field'=>'new_password',
                'label'=>'New Password',
                'rules'=>'trim|required|min_length[6]|max_length[32]|matches[new_password_confirm]'
            ),
            array(
               'field'=>'new_password_confirm',
                'label'=>'Confirm New Password',
                'rules'=>'trim|required|min_length[6]|max_length[32]'
            ),
    ),
);

?>